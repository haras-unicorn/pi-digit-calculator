set windows-shell := ["nu.exe", "-c"]
set shell := ["nu", "-c"]

root := absolute_path('')

default: run

run:
    bend run '{{root}}/src/bend/main.bend'
    
