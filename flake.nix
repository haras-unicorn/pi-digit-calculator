{
  description = "Pi digit calculator - project for learning languages";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { nixpkgs, flake-utils, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
      in
      {
        devShells.default = pkgs.mkShell {
          packages = with pkgs; [
            # Version Control
            git

            # Nix
            nil
            nixpkgs-fmt

            # Bend
            hvm
            bend

            # Spelling
            hunspell
            hunspellDicts.hr-hr
            hunspellDicts.en-us-large

            # Scripts
            just
            nushell

            # Misc
            nodePackages.prettier
            nodePackages.yaml-language-server
            nodePackages.vscode-langservers-extracted
            taplo
            marksman
          ];
        };
      });
}
